app = angular.module('projects',['ui.bootstrap']);

app.controller('projectList', ['$scope', '$window', function($scope, $window) {
  $scope.search = '';
  $scope.includeArchived = false;
  $scope.includeSubprojects = false;
  $scope.projects = $window.projects;
}]);

app.filter("filterArchived", function() {
  return function(projects, includeArchived) {
    filteredProjects = [];
    angular.forEach(projects, function(project, index) {
      if (includeArchived === true) {
        filteredProjects.push(project);
      } else {
        if (project.status === 1) {
          filteredProjects.push(project);
        }
      }
    });
    return filteredProjects;
  };
});

app.filter("filterSubprojects", function() {
  return function(projects, includeSubprojects) {
    filteredProjects = [];
    angular.forEach(projects, function(project, index) {
      if (includeSubprojects === true) {
        filteredProjects.push(project);
      } else {
        if (project.level === 0) {
          filteredProjects.push(project);
        }
      }
    });
    return filteredProjects;
  };
});
